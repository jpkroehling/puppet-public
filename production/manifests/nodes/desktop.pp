node desktop inherits basenode {
	include openjdk7
	include openjdk8
	include maven
	include git
	include sshconfig
	include gitconfig
	include vim
	include thunderbird
	include xchat
	include virt-manager
	include googletalkplugin
	include googlechrome
}
