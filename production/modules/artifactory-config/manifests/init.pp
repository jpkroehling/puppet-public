class artifactory-config {
	file { '/etc/maven/settings.xml':
		mode   => 644,
		owner  => root,
		group  => root,
		source => 'puppet:///modules/artifactory-config/settings.xml',
		ensure => file
	}
}
