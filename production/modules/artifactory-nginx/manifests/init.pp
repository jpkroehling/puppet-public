class artifactory-nginx {
	
	include nginx

	file {'/etc/nginx/conf.d/artifactory.conf':
		ensure  => file,
		source  => 'puppet:///modules/artifactory-nginx/artifactory.conf',
		require => Package['nginx'],
		notify  => Service['nginx'],
	}

	file {'/etc/pki/tls/certs/repo.kroehling.de.bundle.cert':
		ensure  => file,
		source  => 'puppet:///modules/artifactory-nginx/repo.kroehling.de.bundle.cert',
		notify  => Package['nginx'],
	}

	file {'/etc/pki/tls/private/repo.kroehling.de.key':
		ensure  => file,
		source  => 'puppet:///modules/artifactory-nginx/repo.kroehling.de.key',
		notify  => Package['nginx'],
	}
}
