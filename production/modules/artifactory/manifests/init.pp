class artifactory {
	include openjdk7

	yumrepo {'artifactory':
		baseurl  => 'http://dl.bintray.com/jfrog/artifactory-rpms',
		descr    => 'Artifactory Repository',
		enabled  => 1,
		gpgcheck => 0,
		before   => Package['artifactory'],
	}

	package {'artifactory':
		ensure => installed,
	}

	service {'artifactory':
		ensure  => running,
		require => [Package['artifactory'], Package['java-1.7.0-openjdk']],
	}

	file {'/etc/opt/jfrog/artifactory/default':
		ensure => file,
		source => 'puppet:///modules/artifactory/default',
		notify => Service['artifactory'],
	}

	exec {'/bin/firewall-cmd --add-port=8081/tcp --permanent':
		unless => "/bin/firewall-cmd --list-all | grep 8081 > /dev/null",
		notify => Service['firewalld'],
	}
}
