class base {
	include ntp
	include ps1
	include nrsysmond
	include users
	include sudoers
	include sshd
	include puppet
	include screen
	include vim
	include wget
	include yum-updatesd
}
