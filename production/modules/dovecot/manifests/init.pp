class dovecot {
	include mailservercerts
	include firewalld

	package {'dovecot':
		ensure => installed,
	}

	package {'dovecot-pigeonhole':
		ensure => installed,
	}

	package {'dovecot-mysql':
		ensure => installed,
	}

	service {'dovecot':
		ensure  => running,
		enable  => true,
		require => [	File['/etc/pki/tls/private/mail.kroehling.de.pem'],
				File['/etc/pki/tls/certs/mail.kroehling.de.pem'],
				Package['dovecot']],
	}

	file {'/etc/dovecot/dovecot.conf':
		ensure  => file,
		source  => 'puppet:///modules/dovecot/dovecot.conf',
		require => Package['dovecot'],
		notify  => Service['dovecot'],
	}

	file {'/etc/dovecot/dovecot-sql.conf.ext':
		ensure  => file,
		source  => 'puppet:///modules/dovecot/dovecot-sql.conf.ext',
		require => Package['dovecot'],
		notify  => Service['dovecot'],
	}

	file {'/etc/dovecot/conf.d/auth-sql.conf.ext':
		ensure  => file,
		source  => 'puppet:///modules/dovecot/auth-sql.conf.ext',
		require => Package['dovecot'],
		notify  => Service['dovecot'],
	}

	file {'/etc/dovecot/conf.d/10-auth.conf':
		ensure  => file,
		source  => 'puppet:///modules/dovecot/10-auth.conf',
		require => Package['dovecot'],
		notify  => Service['dovecot'],
	}

	file {'/etc/dovecot/conf.d/10-mail.conf':
		ensure  => file,
		source  => 'puppet:///modules/dovecot/10-mail.conf',
		require => Package['dovecot'],
		notify  => Service['dovecot'],
	}

	file {'/etc/dovecot/conf.d/10-master.conf':
		ensure  => file,
		source  => 'puppet:///modules/dovecot/10-master.conf',
		require => Package['dovecot'],
		notify  => Service['dovecot'],
	}

	file {'/etc/dovecot/conf.d/10-ssl.conf':
		ensure  => file,
		source  => 'puppet:///modules/dovecot/10-ssl.conf',
		require => Package['dovecot'],
		notify  => Service['dovecot'],
	}

	file {'/etc/dovecot/conf.d/15-lda.conf':
		ensure  => file,
		source  => 'puppet:///modules/dovecot/15-lda.conf',
		require => Package['dovecot'],
		notify  => Service['dovecot'],
	}

	file {'/etc/dovecot/conf.d/20-imap.conf':
		ensure  => file,
		source  => 'puppet:///modules/dovecot/20-imap.conf',
		require => Package['dovecot'],
		notify  => Service['dovecot'],
	}

	file {'/etc/dovecot/conf.d/20-lmtp.conf':
		ensure  => file,
		source  => 'puppet:///modules/dovecot/20-lmtp.conf',
		require => Package['dovecot'],
		notify  => Service['dovecot'],
	}

	file {'/etc/dovecot/conf.d/20-managesieve.conf':
		ensure  => file,
		source  => 'puppet:///modules/dovecot/20-managesieve.conf',
		require => Package['dovecot'],
		notify  => Service['dovecot'],
	}

	file {'/etc/dovecot/conf.d/90-sieve.conf':
		ensure  => file,
		source  => 'puppet:///modules/dovecot/90-sieve.conf',
		require => Package['dovecot'],
		notify  => Service['dovecot'],
	}

	exec {'/bin/firewall-cmd --add-port=143/tcp --permanent':
		unless => "/bin/firewall-cmd --list-all | grep 143 > /dev/null",
		notify => Service['firewalld'],
	}

	exec {'/bin/firewall-cmd --add-port=993/tcp --permanent':
		unless => "/bin/firewall-cmd --list-all | grep 993 > /dev/null",
		notify => Service['firewalld'],
	}

	exec {'/bin/firewall-cmd --add-port=4190/tcp --permanent':
		unless => "/bin/firewall-cmd --list-all | grep 4190 > /dev/null",
		notify => Service['firewalld'],
	}

}
