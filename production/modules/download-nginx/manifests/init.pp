class download-nginx {
	
	include nginx

	file {'/etc/nginx/conf.d/download.conf':
		ensure  => file,
		source  => 'puppet:///modules/download-nginx/download.conf',
		require => Package['nginx'],
		notify  => Service['nginx'],
	}

	file {'/etc/nginx/conf.d/download_passwd':
		ensure  => file,
		source  => 'puppet:///modules/download-nginx/htpasswd',
		require => Package['nginx'],
		notify  => Service['nginx'],
	}

	file {'/etc/pki/tls/certs/download.kroehling.de.bundle.cert':
		ensure  => file,
		source  => 'puppet:///modules/download-nginx/download.kroehling.de.bundle.cert',
		notify  => Package['nginx'],
	}

	file {'/etc/pki/tls/private/download.kroehling.de.key':
		ensure  => file,
		source  => 'puppet:///modules/download-nginx/download.kroehling.de.key',
		notify  => Package['nginx'],
	}
}
