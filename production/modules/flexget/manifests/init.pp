class flexget {
	include transmission-daemon

	package {'python-pip':
		ensure => installed,
	}

	file {'/var/lib/transmission/.flexget':
		ensure  => directory,
		require => Package['transmission-daemon'],
	}

	file {'/var/lib/transmission/.flexget/config.yml':
		ensure => file,
		source => 'puppet:///modules/flexget/config.yml',
		require => File['/var/lib/transmission/.flexget'],
	}

	cron {'flexget':
		command => '/usr/bin/flexget --cron',
		user    => transmission,
		minute  => 0,
		require => File['/var/lib/transmission/.flexget/config.yml'],
	}

	exec {'/bin/python-pip install flexget transmissionrpc':
		creates => '/usr/bin/flexget',
		require => Package['python-pip'],
	}


}
