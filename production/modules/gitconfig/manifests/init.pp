class gitconfig {
	file { '/home/jpkroehling/.gitconfig':
		ensure => file,
		source => 'puppet:///modules/gitconfig/gitconfig',
		owner  => jpkroehling,
		group  => jpkroehling,
		mode   => 664,
	}
}
