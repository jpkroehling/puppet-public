class googlechrome {

	yumrepo {'google-chrome':
		baseurl  => 'http://dl.google.com/linux/chrome/rpm/stable/x86_64',
		descr    => 'google-talkplugin',
		enabled  => 1,
		gpgcheck => 0,
		before   => Package['google-chrome'],
	}

	package {'google-chrome',
		ensure => installed,
	}

}
