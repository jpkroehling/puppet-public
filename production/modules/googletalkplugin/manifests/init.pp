class googletalkplugin {

	yumrepo {'google-talkplugin':
		baseurl  => 'http://dl.google.com/linux/talkplugin/rpm/stable/x86_64',
		descr    => 'google-talkplugin',
		enabled  => 1,
		gpgcheck => 0,
		before   => Package['google-talkplugin'],
	}

	package {'google-talkplugin',
		ensure => installed,
	}

}
