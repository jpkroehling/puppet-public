class httpd {

	include firewalld 

	package {'httpd':
		ensure => installed,
	}

	service {'httpd':
		ensure  => running,
		enable  => true,
		require => Package['httpd'],
	}

	exec {'/bin/firewall-cmd --add-port=80/tcp --permanent':
		unless => "/bin/firewall-cmd --list-all | grep 80 > /dev/null",
		notify => Service['firewalld'],
	}
}
