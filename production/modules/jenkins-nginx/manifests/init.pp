class jenkins-nginx {
	
	include nginx

	file {'/etc/nginx/conf.d/jenkins.conf':
		ensure  => file,
		source  => 'puppet:///modules/jenkins-nginx/jenkins.conf',
		require => Package['nginx'],
		notify  => Service['nginx'],
	}

	file {'/etc/pki/tls/certs/jenkins.kroehling.de.bundle.cert':
		ensure  => file,
		source  => 'puppet:///modules/jenkins-nginx/jenkins.kroehling.de.bundle.cert',
		notify  => Package['nginx'],
	}

	file {'/etc/pki/tls/private/jenkins.kroehling.de.key':
		ensure  => file,
		source  => 'puppet:///modules/jenkins-nginx/jenkins.kroehling.de.key',
		notify  => Package['nginx'],
	}
}
