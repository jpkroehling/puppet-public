class jenkins {
	include openjdk7
	include openjdk8
	include maven
	include git
	include github-key
	include firewalld

	yumrepo {'jenkins':
		baseurl  => 'http://pkg.jenkins-ci.org/redhat',
		descr    => 'Jenkins Repository',
		enabled  => 1,
		gpgcheck => 0,
		before   => Package['jenkins'],
	}

	package {'jenkins':
		ensure => installed,
	}

	service {'jenkins':
		ensure  => running,
		require => [Package['jenkins'], Package['java-1.7.0-openjdk']],
	}

	file {'/etc/sysconfig/jenkins':
		ensure  => file,
		mode    => 600,
		source  => 'puppet:///modules/jenkins/sysconfig',
		require => Package['jenkins'],
	}

	file {'/var/lib/jenkins/.ssh':
		ensure  => directory,
		mode    => 0600,
		owner   => 'jenkins',
		group   => 'jenkins',
		require => Package['jenkins'],
	}

	file {'/var/lib/jenkins/.ssh/id_rsa':
		ensure  => file,
		mode    => 0600,
		owner   => 'jenkins',
		group   => 'jenkins',
		source  => 'puppet:///modules/jenkins/id_rsa',
		require => File['/var/lib/jenkins/.ssh'],
	}

	file {'/var/lib/jenkins/.ssh/id_rsa.pub':
		ensure  => file,
		mode    => 644,
		owner   => 'jenkins',
		group   => 'jenkins',
		source  => 'puppet:///modules/jenkins/id_rsa.pub',
		require => File['/var/lib/jenkins/.ssh'],
	}

	exec {'/bin/firewall-cmd --add-port=8080/tcp --permanent':
		unless => "/bin/firewall-cmd --list-all | grep 8080 > /dev/null",
		notify => Service['firewalld'],
	}
}
