class jenkinsslave {
	include openjdk7
	include openjdk8
	include maven
	include git
	include github-key
	include bitbucket-key
	include artifactory-config

	user { 'jenkins':
		name       => 'jenkins',
		ensure     => present,
		managehome => true,
	}

	ssh_authorized_key { 'jenkins-key':
		name    => 'jenkins-master',
		key     => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQCbG35/8TsJbbz1yrAkj+lLyOP/L0WD2X2rzxWVXcVEmTiH1Zf5ANL7yD9K7EkbIit0qHQde0+FodpgcrXYObQF6kfEy+sPuxnAHnWYRMc69N7/HReD2RixiZsRuDfHTymAGdomHP1ceuywbQ4dU7b8+E5//19P+8WajMLQ2ujycW3Ts0QhvfYohLZ5FaOlsIY1ospT1mIOEXpxuARS0BpofhQXoXLRD0eCoFEfUVjAWFdTvcD/7F2X8YS2j0v9hBDp42ZTJhJLfaMY4En04JC+RfGFmzyMB7Dr3veeufh3dCkExbG5uZRf51N8+lVVFz4hnOvpmsXuuLUweeaCST3n',
		user    => 'jenkins',
		type    => 'ssh-rsa',
		require => User['jenkins'],
	}

	file { "/home/jenkins/.ssh/id_rsa":
		owner   => jenkins,
		group   => jenkins,
		mode    => 400,
		source  => "puppet:///modules/jenkinsslave/id_rsa",
		require => User['jenkins'],
	}

}
