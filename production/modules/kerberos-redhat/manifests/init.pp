class kerberos-redhat {

	package { "krb5-workstation":
		ensure => installed,
	}

	file { "/etc/krb5.conf":
		source  => "puppet:///modules/kerberos-redhat/krb5.conf", 
		require => Package["krb5-workstation"],
	}
}
