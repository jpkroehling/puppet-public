class mailservercerts {

	file {'/etc/pki/tls/certs/mail.kroehling.de.pem':
		ensure => file,
		mode   => 644,
		source => 'puppet:///modules/mailservercerts/mail.kroehling.de.pem.cert',
	}

	file {'/etc/pki/tls/private/mail.kroehling.de.pem':
		ensure => file,
		mode   => 600,
		source => 'puppet:///modules/mailservercerts/mail.kroehling.de.pem.key',
	}
}
