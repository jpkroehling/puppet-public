class mariadb {
	include firewalld

	package { 'mariadb-server':
		ensure => installed
	}

	service { 'mariadb':
		ensure     => running,
		enable     => true, 
		require    => Package['mariadb-server'],
	}

	exec {'/bin/firewall-cmd --add-port=3306/tcp --permanent':
		unless => "/bin/firewall-cmd --list-all | grep 3306 > /dev/null",
		notify => Service['firewalld'],
	}

}
