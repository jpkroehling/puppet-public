class maven-jboss-config {
	file { '/etc/maven/settings.xml':
		mode    => 644,
		owner   => root,
		group   => root,
		source  => 'puppet:///modules/maven-jboss-config/settings.xml',
		ensure  => file,
		require => Package["maven"],
	}
}
