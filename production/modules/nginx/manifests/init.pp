class nginx {

	include firewalld

	package {'nginx':
		ensure => installed,
	}

	service {'nginx':
		ensure  => running,
		enable  => true,
		require => Package['nginx'],
	}

	exec {'/bin/firewall-cmd --add-port=80/tcp --permanent':
		unless => "/bin/firewall-cmd --list-all | grep 80 > /dev/null",
		notify => Service['firewalld'],
	}

	exec {'/bin/firewall-cmd --add-port=443/tcp --permanent':
		unless => "/bin/firewall-cmd --list-all | grep 443 > /dev/null",
		notify => Service['firewalld'],
	}

}
