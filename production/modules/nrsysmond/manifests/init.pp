class nrsysmond {
	yumrepo { "newrelic":
		baseurl  => 'http://yum.newrelic.com/pub/newrelic/el5/$basearch',
		descr    => 'New Relic packages for Enterprise Linux 5 - $basearch',
		enabled  => 1,
		gpgcheck => 0,
		before   => Package['newrelic-sysmond'],
	}

	package { 'newrelic-sysmond':
		ensure => installed,
		before => File['/etc/newrelic/nrsysmond.cfg'],
	}

	file { '/etc/newrelic/nrsysmond.cfg':
		ensure => file,
		source => "puppet:///modules/nrsysmond/nrsysmond.cfg",
		owner  => root,
		group  => newrelic, 
		mode   => 640,
		before => File['/lib/systemd/system/nrsysmond.service'],
	}

	file { '/lib/systemd/system/nrsysmond.service':
		ensure => file,
		source => 'puppet:///modules/nrsysmond/nrsysmond.service',
		owner  => root,
		group  => root,
		mode   => 640,
	}

	file {'/etc/init.d/newrelic-sysmond':
		ensure => absent,
		before => Service['nrsysmond'],
	}

	service { 'nrsysmond':
		ensure     => running,
		enable     => true,
		subscribe  => File['/lib/systemd/system/nrsysmond.service'],
	}
}
