class ntp {
	package { 'chrony':
		ensure => installed,
	}

	service { 'chronyd':
		ensure    => running,
		require   => Package["chrony"],
		enable    => true,
	}
}

