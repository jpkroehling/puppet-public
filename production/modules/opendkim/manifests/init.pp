class opendkim {
	package {'opendkim':
		ensure => installed,
	}

	service {'opendkim':
		ensure  => running,
		enable  => true,
		require => [Package['opendkim'], File['/etc/opendkim/keys/default.private']],
	}

	file {'/etc/opendkim.conf':
		source  => 'puppet:///modules/opendkim/opendkim.conf',
		mode    => 644,
		require => Package['opendkim'],
	}

	file {'/lib/systemd/system/opendkim.service':
		source  => 'puppet:///modules/opendkim/opendkim.service',
		mode    => 644,
	}

	file {'/etc/opendkim/keys/default.private':
		source  => 'puppet:///modules/opendkim/default.private',
		mode    => 600,
		owner   => 'opendkim',
		group   => 'opendkim',
		require => Package['opendkim'],
	}

	exec {'/bin/systemctl --system daemon-reload':
		subscribe   => File['/lib/systemd/system/opendkim.service'],
		refreshonly => true,
	}
}
