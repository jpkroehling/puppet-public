class openjdk7 {
	package { 'java-1.7.0-openjdk':
		ensure => installed
	}
	package { 'java-1.7.0-openjdk-devel':
		ensure => installed
	}
}
