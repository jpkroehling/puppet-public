class openjdk8 {
	package { 'java-1.8.0-openjdk':
		ensure => installed
	}
	package { 'java-1.8.0-openjdk-devel':
		ensure => installed
	}
}
