class openphoto-nginx {
	
	include nginx
	include openphoto

	file {'/etc/nginx/conf.d/openphoto.conf':
		ensure  => file,
		source  => 'puppet:///modules/openphoto-nginx/openphoto.conf',
		require => Package['nginx'],
		notify  => Service['nginx'],
	}

	file {'/etc/openphoto':
		ensure => directory,
	}

	file {'/etc/openphoto/photos.kroehling.de.ini':
		ensure  => file,
		require => File['/etc/openphoto'],
		source  => 'puppet:///modules/openphoto-nginx/photos.kroehling.de.ini',
	}

	file {'/etc/pki/tls/certs/photos.kroehling.de.bundle.cert':
		ensure  => file,
		source  => 'puppet:///modules/openphoto-nginx/photos.kroehling.de.bundle.cert',
		notify  => Package['nginx'],
	}

	file {'/etc/pki/tls/private/photos.kroehling.de.key':
		ensure  => file,
		source  => 'puppet:///modules/openphoto-nginx/photos.kroehling.de.key',
		notify  => Package['nginx'],
	}
}
