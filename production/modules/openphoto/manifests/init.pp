class openphoto {

	package {'fbida':
		ensure => installed,
	}

	package {'php-pecl-imagick':
		ensure => installed,
	}

	package {'php-gd':
		ensure => installed,
	}

	package {'php-mcrypt':
		ensure => installed,
	}

	package {'php-mysqlnd':
		ensure => installed,
	}
}
