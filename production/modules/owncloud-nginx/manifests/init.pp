class owncloud-nginx {
	include owncloud
	include php-nginx

	file {'/etc/nginx/conf.d/owncloud.conf':
		ensure  => file,
		source  => 'puppet:///modules/owncloud-nginx/owncloud.conf',
		require => Package['nginx'],
		notify  => Service['nginx'],
	}

	file {'/etc/pki/tls/certs/cloud.kroehling.de.bundle.cert':
		ensure  => file,
		source  => 'puppet:///modules/owncloud-nginx/cloud.kroehling.de.bundle.cert',
		notify  => Package['nginx'],
	}

	file {'/etc/pki/tls/private/cloud.kroehling.de.key':
		ensure  => file,
		source  => 'puppet:///modules/owncloud-nginx/cloud.kroehling.de.key',
		notify  => Package['nginx'],
	}

}
