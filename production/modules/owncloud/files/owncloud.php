<?php
$CONFIG = array (
  'installed' => true,
  'dbtype' => 'mysql',
  'dbname' => 'owncloud',
  'dbuser' => 'owncloud',
  'dbpassword' => '...',
  'dbhost' => 'anhanguera',
  'dbtableprefix' => 'oc_',
  'passwordsalt' => '...',
  'forcessl' => false,
  'overwritehost' => '',
  'overwriteprotocol' => '',
  'theme' => '',
  '3rdpartyroot' => '',
  '3rdpartyurl' => '',
  'defaultapp' => 'files',
  'knowledgebaseenabled' => true,
  'knowledgebaseurl' => 'http://api.apps.owncloud.com/v1',
  'appstoreenabled' => true,
  'appstoreurl' => 'http://api.apps.owncloud.com/v1',
  'mail_smtpmode' => 'sendmail',
  'mail_smtphost' => '127.0.0.1',
  'mail_smtpport' => 25,
  'mail_smtpauth' => false,
  'mail_smtpname' => '',
  'mail_smtppassword' => '',
  'appcodechecker' => '',
  'updatechecker' => true,
  'log_type' => 'syslog',
  'loglevel' => '',
  'datadirectory' => '/var/lib/owncloud/data',
  'apps_paths' => 
  array (
    0 => 
    array (
      'path' => '/usr/share/owncloud/apps',
      'url' => '/apps',
      'writable' => false,
    ),
    1 => 
    array (
      'path' => '/var/lib/owncloud/apps',
      'url' => '/apps-appstore',
      'writable' => true,
    ),
  ),
  'instanceid' => '...',
  'version' => '4.90.14',
);

