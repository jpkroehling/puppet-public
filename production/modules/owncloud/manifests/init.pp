class owncloud {

	package {'owncloud':
		ensure => installed,
	}

	file {'/etc/owncloud/config.php':
		ensure  => file,
		require => Package['owncloud'],
	}
}
