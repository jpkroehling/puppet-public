class php-nginx {
	include php
	include nginx

	package {'php-fpm':
		ensure => installed,
	}

	service {'php-fpm':
		ensure  => running,
		enable  => true,
		require => Package['php-fpm'],
	}
}
