class postfix {
	include mailservercerts
	include firewalld

	package {'postfix':
		ensure => installed,
	}

	service {'postfix':
		ensure  => running,
		enable  => true,
		require => [	File['/etc/pki/tls/private/mail.kroehling.de.pem'],
				File['/etc/pki/tls/certs/mail.kroehling.de.pem'],
				Package['postfix']],
	}

	file {'/etc/postfix/main.cf':
		source  => 'puppet:///modules/postfix/main.cf',
		ensure  => file,
		mode    => 644,
		require => Package['postfix'],
		notify  => Service['postfix'],
	}

	file {'/etc/postfix/mysql-virtual-alias-maps.cf':
		source  => 'puppet:///modules/postfix/mysql-virtual-alias-maps.cf',
		ensure  => file,
		mode    => 644,
		require => Package['postfix'],
		notify  => Service['postfix'],
	}

	file {'/etc/postfix/mysql-virtual-mailbox-domains.cf':
		source  => 'puppet:///modules/postfix/mysql-virtual-mailbox-domains.cf',
		ensure  => file,
		mode    => 644,
		require => Package['postfix'],
		notify  => Service['postfix'],
	}

	file {'/etc/postfix/mysql-virtual-mailbox-maps.cf':
		source  => 'puppet:///modules/postfix/mysql-virtual-mailbox-maps.cf',
		ensure  => file,
		mode    => 644,
		require => Package['postfix'],
		notify  => Service['postfix'],
	}

	exec {'/bin/firewall-cmd --add-port=25/tcp --permanent':
		unless => "/bin/firewall-cmd --list-all | grep 25 > /dev/null",
		notify => Service['firewalld'],
	}

	exec {'/bin/firewall-cmd --add-port=465/tcp --permanent':
		unless => "/bin/firewall-cmd --list-all | grep 465 > /dev/null",
		notify => Service['firewalld'],
	}


}
