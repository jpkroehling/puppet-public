class postgresql-wildfly-module {
	include wildfly
	include postgresql-jdbc

	file { '/usr/share/wildfly/standalone/deployments/postgresql-jdbc.jar':
		ensure  => link,
		target  => '/usr/share/java/postgresql-jdbc.jar',
		require => [Package['postgresql-jdbc'], Package['wildfly']],
	}
}
