class postgresql {
	include firewalld

	package { 'postgresql-server':
		ensure => installed
	}

	service { 'postgresql':
		ensure     => running,
		enable     => true,
	}

	file { '/var/lib/pgsql/data/pg_hba.conf':
		owner   => postgres,
		group   => postgres,
		mode    => 600,
		source  => 'puppet:///modules/postgresql/pg_hba.conf',
		require => Package['postgresql-server'],
		notify  => Service['postgresql']
	}

	file { '/var/lib/pgsql/data/postgresql.conf':
		owner   => postgres,
		group   => postgres,
		mode    => 600,
		source  => 'puppet:///modules/postgresql/postgresql.conf',
		require => Package['postgresql-server'],
		notify  => Service['postgresql'],
	}

	exec {'/usr/bin/postgresql-setup initdb':
		creates => '/var/lib/pgsql/data/PG_VERSION',
		before  => [Service['postgresql'], File['/var/lib/pgsql/data/pg_hba.conf'], File['/var/lib/pgsql/data/postgresql.conf']],
		require => Package['postgresql-server'],
	}

	exec {'/bin/firewall-cmd --add-port=5432/tcp --permanent':
		unless => "/bin/firewall-cmd --list-all | grep 5432 > /dev/null",
		notify => Service['firewalld'],
	}
}
