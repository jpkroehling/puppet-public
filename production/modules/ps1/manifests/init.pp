class ps1 {
	file { '/etc/profile.d/ps1.sh':
		ensure  => file,
		source  => "puppet:///modules/ps1/ps1.sh",
		owner   => root, group => root, mode => 755
	}
}
