class puppet {

	package { 'puppet':
		ensure => installed
	}

	service { 'puppetagent':
		enable  => true,
		ensure  => running,
		require => Package['puppet'],
	}

	file {'/etc/puppet/puppet.conf':
		source  => 'puppet:///modules/puppet/puppet.conf',
		ensure  => file,
		owner   => root,
		mode    => 644,
	}
}
