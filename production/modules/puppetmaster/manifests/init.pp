class puppetmaster inherits puppet {

	include firewalld

	package { 'puppet-server':
		ensure => installed
	}

	service { 'puppetmaster':
		enable  => true,
		ensure  => running,
		require => Package['puppet'],
	}

	File['/etc/puppet/puppet.conf'] {
		source => 'puppet:///modules/puppetmaster/puppet.conf',
	}

	exec {'/bin/firewall-cmd --add-port=8140/tcp --permanent':
		unless => "/bin/firewall-cmd --list-all | grep 8140 > /dev/null",
		notify => Service['firewalld'],
	}

}
