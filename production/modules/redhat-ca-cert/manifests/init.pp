class redhat-ca-cert {
	
	file {'/etc/pki/ca-trust/source/anchors/redhat-cacert.crt':
		ensure => file,
		source => 'puppet:///modules/redhat-ca-cert/cacert.crt',
	}

	exec {'/usr/bin/update-ca-trust':
		subscribe => File['/etc/pki/ca-trust/source/anchors/redhat-cacert.crt'],
		noop => true,
	}
}
