class redhat-vpn {
	file { '/etc/NetworkManager/system-connections/Red Hat - AMS':
		ensure  => file,
		source  => "puppet:///modules/redhat-vpn/Red Hat - AMS",
		owner   => root, group => root, mode => 600
	}
	file { '/etc/NetworkManager/system-connections/Red Hat - PHX2':
		ensure  => file,
		source  => "puppet:///modules/redhat-vpn/Red Hat - PHX2",
		owner   => root, group => root, mode => 600
	}
	file { '/etc/NetworkManager/system-connections/Red Hat - RDU':
		ensure  => file,
		source  => "puppet:///modules/redhat-vpn/Red Hat - RDU",
		owner   => root, group => root, mode => 600
	}
}
