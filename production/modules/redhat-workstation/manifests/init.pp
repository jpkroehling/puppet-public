class redhat-workstation {
	include kerberos-redhat
	include redhat-vpn
	include maven-jboss-config
	include libreoffice
	include shutter
	include redhat-ca-cert
	include ekiga
	include openshift-rhc
	include bundler
	include jboss-tools
}
