class rpmfusion {
	package {'rpmfusion-free-release':
		source   => 'http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-20.noarch.rpm',
		provider => 'rpm',
		ensure   => installed,
	}

	package {'rpmfusion-nonfree-release':
		source   => 'http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-20.noarch.rpm',
		provider => 'rpm',
		ensure   => installed,
	}
}
