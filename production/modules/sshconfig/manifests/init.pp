class sshconfig {
	file { '/home/jpkroehling/.ssh/config':
		ensure => file,
		source => 'puppet:///modules/sshconfig/config',
		owner  => jpkroehling,
		group  => jpkroehling,
		mode   => 400,
	}
}
