class sshd {

	package { 'openssh-server':
		ensure => installed,
		before => File['/etc/ssh/sshd_config'],
	}

	file { '/etc/ssh/sshd_config':
		ensure => file,
		source => 'puppet:///modules/sshd/sshd_config',
		owner  => root,
		group  => root, 
		mode   => 600,
	}

	service { 'sshd':
		ensure     => running,
		enable     => true,
		subscribe  => File['/etc/ssh/sshd_config'],
	}

	file { '/etc/ssh/ssh_known_hosts':
		mode   => 644,
		owner  => root,
		group  => root,
		ensure => file
	}
}
