class transmission-daemon {

	include firewalld

	package {'transmission-daemon':
		ensure => installed,
	}

	service {'transmission-daemon':
		ensure  => running,
		enable  => true,
		require => Package['transmission-daemon'],
	}

	file {'/var/lib/transmission/.config/transmission-daemon/settings.json':
		ensure  => file,
		source  => 'puppet:///modules/transmission-daemon/settings.json',
		require => Package['transmission-daemon'],
	}

	file {'/var/lib/transmission/.config/transmission-daemon/blocklist-urls.txt':
		ensure  => file,
		source  => 'puppet:///modules/transmission-daemon/blocklist-urls.txt',
		require => Package['transmission-daemon'],
	}

	file {'/usr/local/bin/update-blocklists':
		ensure => file,
		mode   => 755,
		source => 'puppet:///modules/transmission-daemon/update-blocklists',
	}

	cron {'update-blocklists':
		command => '/usr/local/bin/update-blocklists',
		user    => root,
		hour    => 2,
		minute  => 0,
		require => File['/usr/local/bin/update-blocklists'],
	}

        exec {'/bin/firewall-cmd --add-port=9091/tcp --permanent':
                unless => "/bin/firewall-cmd --list-all | grep 9091 > /dev/null",
                notify => Service['firewalld'],
        }

}
