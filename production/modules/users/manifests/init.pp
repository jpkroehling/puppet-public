class users {

	user {'jpkroehling':
		managehome => true,
		gid        => 'wheel',
		ensure     => present,
	}

	ssh_authorized_key { 'jpkroehling-key':
		name    => 'jpkroehling',
		key     => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQCzxnkseL9/QO2rkwBAizutLaxJNM4IB/EkK2+ZH4ajgRyaC42MBmYh0lfqvDRWSCTq5fw2dhGuvBGEWCSP5WfxpyHl4EddZiX0m+Ps5NA6TbEm7nyZ4OGhmz7SdoTiXQXaKqw4FUQlqC7I8S8KqO0RN1O4exmAXss2NjS0dpmSlsQbKrkpMWF1HC7ZoOhEuIOu4PB+cyuexHIfNWXwnnbZ2d4kAOULLSAB/ivqS+cqO5ZZerYasmZ+7sQSOWtgHWbW2EjngH/ySgK1wAiSD1eg2eYjJFhj+zCArQqa6BaJgQ3DNI1cFjfj7vLed8dAcyqycZdlkxCa44nKchE7MslD',
		user    => 'jpkroehling',
		type    => 'ssh-rsa',
		require => User['jpkroehling'],
	}
}
