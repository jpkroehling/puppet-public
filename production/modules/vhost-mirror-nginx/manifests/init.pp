class vhost-mirror-nginx {
	
	include nginx

	file {'/etc/nginx/conf.d/mirror.conf':
		ensure  => file,
		source  => 'puppet:///modules/vhost-mirror-nginx/mirror.conf',
		require => Package['nginx'],
		notify  => Service['nginx'],
	}

	file {'/etc/pki/tls/certs/mirror.kroehling.de.bundle.cert':
		ensure  => file,
		source  => 'puppet:///modules/vhost-mirror-nginx/mirror.kroehling.de.bundle.cert',
		notify  => Package['nginx'],
	}

	file {'/etc/pki/tls/private/mirror.kroehling.de.key':
		ensure  => file,
		source  => 'puppet:///modules/vhost-mirror-nginx/mirror.kroehling.de.key',
		notify  => Package['nginx'],
	}
}
