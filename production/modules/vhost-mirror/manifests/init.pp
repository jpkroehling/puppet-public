class vhost-mirror {

	include httpd

	file {'/etc/httpd/conf.d/mirror.conf':
		ensure  => file,
		source  => 'puppet:///modules/vhost-mirror/vhost-mirror.conf',
		require => Package['httpd'],
		notify  => Service['httpd'],
	}

	exec {'/sbin/setsebool httpd_can_network_connect -P on':
		unless => "/sbin/getsebool httpd_can_network_connect | grep -v '> off' > /dev/null",
	}


}
