class virt-manager {
	package { "virt-manager":
		ensure => installed,
	}
	package { "qemu-system-x86":
		ensure => installed,
	}
	package { "libvirt-daemon-kvm":
		ensure => installed,
	}
	package { "libvirt-daemon-config-network":
		ensure => installed,
	}
}
