class vmailuser {

	user {'vmail':
		managehome => true,
		home       => '/var/spool/mail',
		ensure     => present,
	}

}
