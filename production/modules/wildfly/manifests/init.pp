class wildfly {
	include firewalld

	package {'wildfly':
		ensure => installed,
	}

	service {'wildfly':
		ensure => running,
		require => Package['wildfly'],
	}

	file {'/etc/wildfly/domain/mgmt-users.properties':
		ensure  => file,
		source  => 'puppet:///modules/wildfly/mgmt-users.properties',
		require => Package['wildfly'],
		mode    => 600,
		owner   => 'wildfly',
		group   => 'wildfly',
	}

	file {'/etc/wildfly/standalone/mgmt-users.properties':
		ensure  => file,
		source  => 'puppet:///modules/wildfly/mgmt-users.properties',
		require => Package['wildfly'],
		mode    => 600,
		owner   => 'wildfly',
		group   => 'wildfly',
	}

	exec {'/bin/firewall-cmd --add-port=8080/tcp --permanent':
		unless => "/bin/firewall-cmd --list-all | grep 8080 > /dev/null",
		notify => Service['firewalld'],
	}

}
