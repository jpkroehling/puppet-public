class xbmc {
	include rpmfusion

	package {'xbmc':
		ensure  => installed,
		require => Package['rpmfusion-free-release'],
	}

	package {'vdr-vnsiserver3':
		ensure  => installed,
		require => Package['rpmfusion-free-release'],
	}

	package {'dvb-apps':
		ensure => installed,
	}

	service {'vdr':
		ensure  => stopped,
		require => Package['vdr-vnsiserver3'],
	}

	exec {'/bin/scandvb -x 0 -o vdr -a 0 /usr/share/dvb/dvb-c/de-Muenchen  > /etc/vdr/channels.conf':
		unless => '/bin/test -s /etc/vdr/channels.conf',
	}
}
