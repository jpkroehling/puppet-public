class xchat {
	package { "xchat":
		ensure => installed,
	}

	file {'/home/jpkroehling/.config/xchat2/xchat.conf':
		ensure => file,
		source => 'puppet:///modules/xchat/xchat.conf',
		owner  => jpkroehling,
		group  => jpkroehling,
		mode   => 600,
	}

	file {'/home/jpkroehling/.config/xchat2/servlist_.conf':
		ensure => file,
		source => 'puppet:///modules/xchat/servlist_.conf',
		owner  => jpkroehling,
		group  => jpkroehling,
		mode   => 600,
	}
}
