class yum-repo-mirror {
	yumrepo {'fedora':
		baseurl  => 'http://mirror.kroehling.de/pub/fedora/linux/releases/$releasever/Everything/$basearch/os/',
		enabled  => 1,
		gpgcheck => 1,
		gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch',
		descr    => 'Fedora $releasever - $basearch',
	}

	yumrepo {'fedora-updates':
		baseurl  => 'http://mirror.kroehling.de/pub/fedora/linux/updates/$releasever/$basearch/',
		enabled  => 1,
		gpgcheck => 1,
		gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch',
		descr    => 'Fedora $releasever - $basearch',
	}
	
	yumrepo {'fedora-updates-testing':
		baseurl  => 'http://mirror.kroehling.de/pub/fedora/linux/updates/testing/$releasever/$basearch/',
		enabled  => 0,
		gpgcheck => 1,
		gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch',
		descr    => 'Fedora $releasever - $basearch',
	}
}
