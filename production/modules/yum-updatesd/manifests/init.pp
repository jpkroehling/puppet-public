class yum-updatesd {

	package {'yum-updatesd':
		ensure => installed,
	}

	service {'yum-updatesd':
		ensure => running,
		enable => true,
		require => Package['yum-updatesd'],
	}

	file {'/etc/yum/yum-updatesd.conf':
		ensure => file,
		notify => Service['yum-updatesd'],
	}
}
